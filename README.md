# ternopil-form-filler

## Getting started

1. Download and install Node from https://nodejs.org/
2. Download webdriver from https://chromedriver.chromium.org/ and unzip it to the project directory.
3. Put your data file in the project directory. Name should be today date in dd.mm.yyyy.xlsx format or you can change it in config.
4. Columns in .xslx should be in specific order:
   | Number | Full name including patronymic | birthdate in dd.mm.yyyy format | phone number (10 last digits) | empty | gender |
   |--------|--------------------------------|--------------------------------|-------------------------------|-------|--------|
   | 1 | Піпко Кіндрат Акакійович | 31.12.2020 | 0971234567 | | ч |

5. Run start.bat or type "npm start" in command line in project directory.
