const {
  fixNameExtraSpaces,
  validateDate,
  validatePhoneNumber,
  validateGender,
} = require("./validators");

class Person {
  constructor(arr) {
    this.id = arr[0];
    this.fullName = fixNameExtraSpaces(arr[1]);
    this.birthDate = validateDate(arr[2]);
    this.phoneNumber = validatePhoneNumber(arr[3]);
    this.gender = validateGender(arr[5]);
  }
}

module.exports = Person;
