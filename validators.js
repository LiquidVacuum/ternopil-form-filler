function dialog(message) {
  const readlineSync = require("readline-sync");

  if (readlineSync.keyInYN(message)) {
    process.exit();
  } else {
    console.log("ok");
  }
}

const fixNameExtraSpaces = (name) => {
  const fixed = name.split(" ").filter((e) => e);
  if (fixed.length < 3) {
    console.log(name);
    dialog(`Name seems not full. Do you want to exit and fix your file?`);
  }
  return fixed.join(" ");
};

const validatePhoneNumber = (str) => {
  const numberIsValid = /^\d{10}$/.test(str);
  if (!numberIsValid) {
    dialog(
      `Phone number ${str} might be incorrect. Do you want to exit and fix your file?`
    );
  }
  return str || "-";
};

const validateDate = (str) => {
  if (!str) return;
  let date = str.split(".").filter((e) => e);

  if (
    date.length === 3 &&
    date[0].length <= 2 &&
    date[1].length <= 2 &&
    date[2].length === 4 &&
    +date[0] > 0 &&
    +date[0] <= 31 &&
    +date[1] > 0 &&
    +date[1] <= 12
  ) {
    return date.map((e) => e.padStart(2, "0")).join("");
  } else {
    console.log(`BirthDate ${str} is invalid, fix or remove it and try again`);
    process.exit();
  }
};

const validateGender = (str) => {
  switch (str) {
    case "Ж":
    case "ж":
    case "F":
    case "f":
      return "female";
      break;
    case "М":
    case "м":
    case "m":
    case "M":
    case "ч":
    case "Ч":
      return "male";
      break;
    default:
      console.log(`Gender ${str} is invalid, fix it and try again`);
      process.exit();
  }
};

module.exports = {
  fixNameExtraSpaces: fixNameExtraSpaces,
  validatePhoneNumber: validatePhoneNumber,
  validateDate: validateDate,
  validateGender: validateGender,
};
