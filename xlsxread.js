const XLSX = require("xlsx");

const xlsxRead = (filename) => {
  console.log("\x1b[33m%s\x1b[0m", `Opening file ${filename}\n`);

  const readOpts = {
    // <--- need these settings in readFile options
    cellText: false,
    cellDates: true,
  };

  const jsonOpts = {
    header: 1,
    defval: "",
    blankrows: true,
    raw: false,
    dateNF: 'dd"/"mm"/"yyyy', // <--- need dateNF in sheet_to_json options (note the escape chars)
  };

  const workbook = XLSX.readFile(filename, readOpts);

  const worksheet = workbook.Sheets[workbook.SheetNames[0]]; // pick first sheet in xlsx
  // const worksheet = workbook.Sheets['Sheet1']; // or pick a sheet by name

  const json = XLSX.utils.sheet_to_json(worksheet, jsonOpts);

  return json;
};

exports.xlsxRead = xlsxRead;
