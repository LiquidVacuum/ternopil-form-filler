const { Builder, Browser, By, Key, until } = require("selenium-webdriver");
const config = require("./config");

class FormModel {
  constructor() {
    const now = new Date();
    this.todayDate = `${now.getDate().toString().padStart(2, "0")}${(
      now.getMonth() + 1
    )
      .toString()
      .padStart(2, "0")}${now.getFullYear()}`;
  }

  fillForm = async (person, failedLinesArray) => {
    try {
      this.driver = await this.buildDriver();
      await this.driver.get(config.formURL);
      await this.fillGender(person);
      await this.fillFullName(person);
      await this.fillPhoneNumber(person);
      await this.fillReceiveDate(person);
      await this.fillBirthDate(person);
      await this.fillPlaceList(person);

      await this.driver.sleep(500);
      await this.pressSubmitButton(person);

      // waiting until form sent
      await this.driver.wait(until.urlContains("formResponse"), 30000);

      console.log("\x1b[32m%s\x1b[0m", `+ ${person.id} ${person.fullName}`);
    } catch (e) {
      console.log("\x1b[41m%s\x1b[0m", `- ${person.id} ${person.fullName}`);
      failedLinesArray.push(person);
      await this.driver.sleep(5000);
    } finally {
      await this.driver.quit();
    }
  };

  buildDriver = async () => {
    //Adding options to remove "DevTools listening on ws://127.0.0.1" messages
    const chrome = require("selenium-webdriver/chrome");
    const options = new chrome.Options();
    options.excludeSwitches("enable-logging");
    options.addArguments("--lang=en-GB");
    if (config.invisibleMode) options.headless();

    return await new Builder()
      .forBrowser(Browser.CHROME)
      .withCapabilities(options)
      .build();
  };

  fillGender = async (person) => {
    let radio;
    if (person.gender === "male") {
      radio = config.formXPath.male;
    } else if (person.gender === "female") {
      radio = config.formXPath.female;
    } else {
      throw new Error(
        `Invalid gender, line ${person.id}. Supported: Ж ж F f М м m M ч Ч`
      );
    }
    await this.driver.findElement(By.xpath(radio)).click();
  };

  fillFullName = async (person) => {
    await this.driver
      .findElement(By.xpath(config.formXPath.fullName))
      .sendKeys(`${person.fullName}`, Key.RETURN);
  };

  fillPhoneNumber = async (person) => {
    await this.driver
      .findElement(By.xpath(config.formXPath.phoneNumber))
      .sendKeys(`${person.phoneNumber}`, Key.RETURN);
  };

  fillReceiveDate = async () => {
    this.driver
      .findElement(By.xpath(config.formXPath.receiveDate))
      .sendKeys(`${this.todayDate}`, Key.RETURN);
  };

  fillPlaceList = async () => {
    // await this.driver.findElement(By.xpath(config.formXPath.placeList)).click(); // doesnt work
    // await this.driver.findElement(By.xpath(config.formXPath.placeYWAM)).click(); // doesnt work

    await this.driver
      .wait(until.elementLocated(By.xpath(config.formXPath.placeList)), 10000)
      .click();

    let dropdownItem = await this.driver
      .wait(until.elementLocated(By.xpath(config.formXPath.place)), 10000)
      .click();
  };

  fillBirthDate = async (person) => {
    if (!person.birthDate) return;

    this.driver
      .findElement(By.xpath(config.formXPath.birthDate))
      .sendKeys(`${person.birthDate}`, Key.RETURN);
  };

  pressSubmitButton = async () => {
    await this.driver
      .wait(until.elementLocated(By.xpath(config.formXPath.sendButton)), 10000)
      .click();
  };
}

module.exports = FormModel;
