// Paste link to your form here.
const formURL =
  "https://docs.google.com/forms/d/e/1FAIpQLSfTSJ61y789-W6WWohHZIFP1UbzE8v_ws0ETXhlyTuOvqi_sw/viewform";

// If xlsx path is empty it looks for a file with today date like this: 31.12.2020.xlsx
// const xlsxPath = "";
// To use specific file name paste it like this:
// const xlsxPath = "./filename.xlsx";
const xlsxPath = "";

// These are links to specific fields of the form.
// You may need to modify them if google makes some changes in the form page structure or in case of fields order change.
const formXPath = {
  fullName:
    '//*[@id="mG61Hd"]/div[2]/div/div[2]/div[1]/div/div/div[2]/div/div[1]/div/div[1]/input',
  male: '//*[@id="i9"]',
  female: '//*[@id="i12"]',
  birthDate:
    '//*[@id="mG61Hd"]/div[2]/div/div[2]/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/input',
  phoneNumber:
    '//*[@id="mG61Hd"]/div[2]/div/div[2]/div[4]/div/div/div[2]/div/div[1]/div/div[1]/input',
  placeList:
    '//*[@id="mG61Hd"]/div[2]/div/div[2]/div[5]/div/div/div[2]/div/div[1]/div[1]/div[1]/span',
  place:
    '//*[@id="mG61Hd"]/div[2]/div/div[2]/div[5]/div/div/div[2]/div/div[2]/div[11]',
  receiveDate:
    '//*[@id="mG61Hd"]/div[2]/div/div[2]/div[6]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/input',
  sendButton: '//*[@id="mG61Hd"]/div[2]/div/div[3]/div[1]/div[1]/div/span',
};

//true - do not open chrome windows
//false - open windows to see all the magic
const invisibleMode = true;

console.log("\x1b[33m%s\x1b[0m", "Config loaded successfully");

module.exports = {
  formURL: formURL,
  formXPath: formXPath,
  xlsxPath: xlsxPath,
  invisibleMode: invisibleMode,
};
