const xlsxRead = require("./xlsxread");
const config = require("./config");
const FormModel = require("./form-model");
const Person = require("./person");

const table = xlsxRead.xlsxRead(getTableFileName());
const personTable = table.filter((e) => !!e[1]).map((e) => new Person(e));

console.log(
  "\x1b[33m%s\x1b[0m",
  `Starting form filling. ${personTable.length} people in total.\n`
);

fillEveryPerson(personTable);

async function fillEveryPerson(array) {
  const failedLines = [];
  for (const i of array) {
    await new FormModel().fillForm(i, failedLines);
  }
  if (failedLines.length > 0) {
    fillEveryPerson(failedLines);
  } else {
    console.log("\x1b[33m%s\x1b[0m", `\n✓ Form filling finished.`);
  }
}

function getTableFileName() {
  if (config.xlsxPath) return config.xlsxPath;
  const now = new Date();
  const todayDate = `${now.getDate().toString().padStart(2, "0")}.${(
    now.getMonth() + 1
  )
    .toString()
    .padStart(2, "0")}.${now.getFullYear()}`;
  return `./${todayDate}.xlsx`;
}
